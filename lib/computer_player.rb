class ComputerPlayer
  attr_accessor :mark, :board, :pos, :name
  def initialize(name= "I'm smarter than you")
    @name = name
    @mark = :O
    @board = board
  end

  def display(board)
    @board = board
  end

  def get_move
    (0..2).each do |pos1|
      (0..2).each do |pos2|
        @pos = [pos1, pos2]
        if @board.empty?(@pos) && @board.place_mark(@pos, @mark)
          if @board.winner
            # @board.place_mark(@pos, nil)
            return [pos1, pos2]
          else
            @board.place_mark(@pos, nil)
          end
        end
      end
    end

    (0..2).each do |pos1|
      (0..2).each do |pos2|
        @pos = [pos1, pos2]
        if @board.empty?(@pos) && @board.place_mark(@pos, :X)
          if @board.winner
            @board.place_mark(@pos, nil)
            @board.place_mark(@pos, @mark)
            return [pos1, pos2]
          else
            @board.place_mark(@pos, nil)
          end
        end
      end
    end

    # until @board.place_mark(@pos, @mark)
    #     pos1 = rand(0..2)
    #     pos2 = rand(0..2)
    #     @pos = [pos1, pos2]
    #     return [pos1, pos2] if @board.place_mark(@pos, @mark)

    (0..2).each do |pos1|
      (0..2).each do |pos2|
        pos1 += rand(0..2).floor if pos1 == 0
        pos1 += rand(0..1).floor if pos1 == 1
        pos1 -= rand(0..2).floor if pos1 == 2
        pos2 += rand(0..2).floor if pos2 == 0
        pos2 += rand(0..1).floor if pos2 == 1
        pos2 -= rand(0..2).floor if pos2 == 2
        @pos = [pos1, pos2]
        return [pos1, pos2] if board.place_mark(@pos, @mark)
      end
    end
    # end
    # empty_spaces = []
    # @board.grid.each { |pos| empty_spaces << pos if pos == nil }
    # empty_spaces.shuffle[0]
    # p empty_spaces
  end
end
