require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'

# current_player
# switch_players!
# play_turn, which handles the logic for a single turn
# play, which calls play_turn each time through a loop until the game is over


class Game
  attr_accessor :board, :current_player, :player_one, :player_two, :name
  def initialize(player_one, player_two)
    @board = Board.new
    @name = name
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
  end

  def switch_players!
    return @current_player = @player_two if @current_player == @player_one
    @current_player = @player_one if @current_player == @player_two
    play_turn unless @board.over? || @board.winner
  end

  def play_turn
    #@player_one = HumanPlayer.new(player_one)#(name)#(@name)
    #@player_two = ComputerPlayer.new(player_two)#(@name)
    @current_player.display(board)
    puts ""
    if @current_player == @player_one
      pos = @player_one.get_move
      @board.place_mark(pos, :X)
    else
      pos = @player_two.get_move
      @board.place_mark(pos, :O)
    end
    # @current_player.display(board)
    switch_players!

  end

  def play
    until @board.winner || @board.over?
      play_turn
    end
    winner = @board.winner
    if winner
      if winner == :O
        @player_one.display(board)
        puts "\nCOMPUTER WINS!!"
      else
        @player_one.display(board)
        puts "\nYOU WIN!!"
      end
    else
      @player_one.display(board)
      puts "\nIT'S A TIE!"
    end
  end

end

if __FILE__ ==$PROGRAM_NAME
  player_one = HumanPlayer.new
  player_two = ComputerPlayer.new
  this_game = Game.new(player_one, player_two)
  this_game.play
end
