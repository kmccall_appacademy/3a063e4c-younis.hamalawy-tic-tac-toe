class Board
  attr_accessor :grid, :pos, :mark

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @pos = pos
    @mark = mark
    pos1 = pos[0]
    pos2 = pos[1]
    if mark.nil?
      @grid[pos1][pos2] = nil
      return nil
    end
    if empty?(pos)
      @grid[pos1][pos2] = mark.to_sym
      return @grid[pos1][pos2]
    end
  end

  def empty?(pos)
    @pos = pos
    pos1 = pos[0]
    pos2 = pos[1]
    return true if @grid[pos1][pos2].nil?
    false
  end

  def winner
    return @grid[0][0] if @grid[0][0] == @grid[1][0] && @grid[1][0] == @grid[2][0] && @grid[0][0]
    return @grid[0][1] if @grid[0][1] == @grid[1][1] && @grid[1][1] == @grid[2][1] && @grid[0][1]
    return @grid[0][2] if @grid[0][2] == @grid[1][2] && @grid[1][2] == @grid[2][2] && @grid[0][2]
    return @grid[0][0] if @grid[0][0] == @grid[0][1] && @grid[0][1] == @grid[0][2] && @grid[0][0]
    return @grid[1][0] if @grid[1][0] == @grid[1][1] && @grid[1][1] == @grid[1][2] && @grid[1][0]
    return @grid[2][0] if @grid[2][0] == @grid[2][1] && @grid[2][1] == @grid[2][2] && @grid[2][0]
    return @grid[0][0] if @grid[0][0] == @grid[1][1] && @grid[1][1] == @grid[2][2] && @grid[0][0]
    return @grid[0][2] if @grid[0][2] == @grid[1][1] && @grid[1][1] == @grid[2][0] && @grid[0][2]
    nil
  end

  def over?
    return true if winner
    !winner && @grid.flatten.all?
  end
end
