class HumanPlayer
  attr_accessor :mark, :board
  attr_reader :name
  def initialize(name= "I am human")
    @name = name
    # puts 'Please enter your name:'
    # name = gets.chomp
    @board = board
    @mark = :X
  end

  def display(board)
    @board = board
    board.grid.each { |e| puts e.inspect }
  end

  def get_move
    @board = board
    valid_moves = [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]
    valid_move = nil

    until valid_moves.include?(valid_move) && board.empty?(valid_move)
      puts 'Please enter where to place mark: "ex. 1,1"'
      move = gets.chomp
      valid_move = [(move[0].to_i - 1), (move[-1].to_i - 1)]

      if valid_moves.include?(valid_move) && board.empty?(valid_move)
        return valid_move
      else
        puts 'This is not a valid move!'
      end
    end
  end
end
